# ReactJs test component template

# Результат
Компонент-контейнер с графиком, таблицей и кнопкой, готовый к использованию:
```
import testComponent from ',,/path/to/component/component';
...
	<testComponent />
...
```

# Пояснения 
## График
Аналогичный этому https://www.highcharts.com/demo/column-stacked, с произвольными данными (инициализируем в редьюсере).
Используем react-highcharts.

## Таблица
Произвольные данные (инициализируем в редьюсере), используем react-bootstrap-table

## Кнопка
По клику на кнопке меняем данные графика и таблицы в store на
произвольные.

## Вёрстка
Bootstrap 4, используем reactstrap http://reactstrap.github.io/.
