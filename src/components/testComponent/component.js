import React, { Component } from 'react';
import { connect } from 'react-redux';

import store from '../../store';
import { setTestComponentProperty } from '../../actions/testComponent/actions';


/* Компонент - отображение */
class testComponentView extends Component {

  render() {
    const { chartData, tableData,
            onButtonClick } = this.props;

    return (
      /* Собственно здесь рисуем график, таблицу и кнопку*/
    )
  }
}


/* Контейнер - здесь логика изменения состояния компонента */
class testComponent extends Component {

  onButtonClick = e => {
    store.dispatch(setTestComponentProperty('Новое значение'));

    /*
      Здесь задаем новые данные для графика и таблицы
    */
  }

  chartOptions = data => {
    // готовим данные для графика
  };

  tableOptions = data => {
    // готовим данные для таблицы
  };

  render() {
    const { chartData, tableData } = this.props.data;

    return (
      <testComponentView  chartOptions={this.chartOptions(chartData)}
                          tableOptions={this.tableOptions(tableData)}
                          onButtonClick={this.onButtonClick} />
    )
  }
}


const mapStateToProps = function(store) {
  return {
    data: store.testComponentState.testComponentData
  };
}


export default connect(mapStateToProps)(testComponent);
