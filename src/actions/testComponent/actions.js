import * as types from '../action-types';


export function setTestComponentProperty(data) {
  return {
    type: types.SET_TEST_COMPONENT_PROPERTY,
    data
  };
}


/* Добавляем свои actions */
