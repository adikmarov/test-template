import { combineReducers } from 'redux';

import testComponentReducer from './testComponent/reducers';


var reducers = combineReducers({
    testComponentState: testComponentReducer,
});


export default reducers;
